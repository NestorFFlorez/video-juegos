$(function(){
  $("[data-toggle='tooltip']").tooltip();
  $("[data-toggle='popover']").popover();
  $('.carousel').carousel ({
    interval: 7500
  });
  $('#contacto').on('show.bs.modal', function (e){
    console.log('El Modal Contacto se esta Mostrando');

    $('#contactoBtn').removeClass('btn-outline-success');
    $('#contactoBtn').addClass('btn-primary');
    $('#contactoBtn').prop('desabled', true);

  });
  $('#contacto').on('shown.bs.modal', function (e){
    console.log('El Modal Contacto se Mostró');
  });
  $('#contacto').on('hide.bs.modal', function (e){
    console.log('El Modal Contacto se Oculta');
  });
  $('#contacto').on('hidden.bs.modal', function (e){
    console.log('El Modal Contacto se Ocultó');

    $('#contactoBtn').prop('desabled', false);

  });
});
